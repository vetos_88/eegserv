
from django.conf.urls import url, include
from django.contrib import admin
# from eeg_serv.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
	url(r'^eeg/', include("eeg_serv.urls")),
]
