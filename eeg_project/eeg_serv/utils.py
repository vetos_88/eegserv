from django.core.exceptions import ObjectDoesNotExist
from django.http import *
from eeg_serv.models import User, Track
import re
import unicodecsv
from django.http import HttpResponse


def valid_data(data):
    """
     Проверка корректности присланных данных
     данные в виде строки должны соответствовать шаблону(формату)
     1,1,1,1|1,1,1,1|1,1,1,1 ....
    """
    #  в идеале заменить регуляркой

    patt = re.compile(r"\s+")
    data = patt.sub("", data)
    array_arrays = data.split('|')
    array_arrays = [list(map(int, arr.split(','))) for arr in array_arrays]  # ValueError,
    for arr in array_arrays:
        if len(array_arrays[0]) != len(arr) or len(arr) < 4:
            raise TypeError
    return data


def receive_data(request, active_users, track_chunked_id):
    """
    :param request: запрос должен содержать следующие хедеры:
        PACKAGETYPE:
            обозначает тип пакета с данными мользователя
            значение может быть только:
                - start - стартовый пакет. Означает что пользователь начал передавать данные.
                - middle -  промежуточные пакеты.
                - end - завершающий пакет.
                после передачи PACKAGETYPE == 'end' трек удаляеться из track_chunked_id
                (возможность дозаписи убираеться).
            Обязательно должен содержать хедер DATASIZE.
        DATASIZE:
            польный размер данных которые будут переданны. Обязательно должен быть конверируемым в тип int()

        NUMPACKAGE:
            номер пакета. пакет 'start' имеет номер 0.
        Запрос может содержать токен если он есть его значение используеться для проверки авторизации,
        если нет, трек пишеться от имени анонимного пользователя.

    :param active_users: множество авторизированных пользователей
    :param track_chunked_id: словарь содержащий треки разбитые на пакеты.

    """

    ANONUSER = "anonymous"
    response = ""

    if not request.POST.get("data"):
        return HttpResponse("No data in request",
                            status=200,
                            content_type='text/plain')

    data = request.POST["data"]

    try:
        data = valid_data(data)
    except (ValueError, TypeError):
        response = "Invalid data format "
        return HttpResponseBadRequest(response,
                                      status=400,
                                      content_type='text/plain')

    print(data)

    regex = re.compile(r"^HTTP_")
    http_headers = dict((regex.sub('', header), value) for (header, value)
                        in request.META.items() if header.startswith('HTTP_')
                        or header.startswith('CONTENT_'))

    login = request.POST["token"] if request.POST.get("token") else ANONUSER  # анонимный или нет пользователь

    if login != ANONUSER and login not in active_users:  # авторизирован или нет
        response = "Please authorize first."
        return HttpResponseForbidden(response,
                                     status=401,
                                     content_type='text/plain')

    try:
        user = User.objects.get(login=login)  # есть ли в базе данных. Возможно избыточна т.к.
    except ObjectDoesNotExist:  # проверка есть в функиции авторизации
        response = "User not found."
        return HttpResponseNotFound(response)

    # if not data_is_valid(data):
    #     response = "Invalid data format"
    #     return HttpResponseBadRequest(response,
    #                                   status=422)

    try:
        packagetype = http_headers["PACKAGETYPE"]  # проверка на наличие хедера PACKAGETYPE
    except KeyError:
        response = "Provide package type"
        return HttpResponseBadRequest(response,
                                      content_type='text/plain')

    pack_types = ["start", "middle", "end"]
    if packagetype not in pack_types:
        response = "No serial package. Provide package type."
        return HttpResponseBadRequest(response,
                                      content_type='text/plain')

    if packagetype == "start":
        print("start pack")

        try:
            datasize = int(http_headers["DATASIZE"])
        except (KeyError, ValueError):
            response = "Package size is indefined"
            return HttpResponseBadRequest(response,
                                          content_type='text/plain')

        if datasize > 200:
            response = "Data too large."
            return HttpResponseBadRequest(response, status=413,
                                          content_type='text/plain')

        # Коментари ТОЛЬКО ASCII
        # track_comment = http_headers["COMMENT"] if http_headers.get("COMMENT") else "no comment"
        # track_comment = track_comment.encode(encoding='utf-8')
        # print(track_comment)
        track_param = {"user": user,
                       "track_comment": http_headers["COMMENT"] if http_headers.get("COMMENT") else "no comment",
                       "track_data": data}
        user_track = Track(**track_param)
        user_track.save()
        track_id = user_track.id
        track_chunked_id[login] = track_id
        response = "Start data chunk from user {} was received".format(login)
        return HttpResponse(response,
                            status=202,
                            content_type='text/plain')
    else:
        numpackage = http_headers["NUMPACKAGE"]
        try:
            track_id = track_chunked_id[login]
        except KeyError:
            response = "No serial package. Please provide 'start' package."
            return HttpResponseBadRequest(response,
                                          content_type='text/plain')
        try:
            track = Track.objects.get(id=track_id)
        except ObjectDoesNotExist:
            response = "Track not found."
            return HttpResponseNotFound(response,
                                        content_type='text/plain')
        data = "|" + data
        track.track_data += data
        if http_headers.get("COMMENT"):
            track.track_comment = http_headers.get("COMMENT")
        track.save()
        if packagetype == "middle":
            print("middle_pack")
            response = "Middle data no {} chunk from user {} was received".format(numpackage, login)
            return HttpResponse(response, status=202,
                                content_type='text/plain')
        else:
            print("end pack")
            track_chunked_id.pop(login, "no user")  # добавть проверку на присутствие трека в записываемом множестве
            response += "Track from user {} was added".format(login)
            return HttpResponse(response, status=201,
                                content_type='text/plain')


def export_as_csv_action(description="Export selected objects as CSV file",
                         fields=None, exclude=None, header=True):
    def export_as_csv(modeladmin, request, queryset):

        opts = modeladmin.model._meta

        if not fields:
            field_names = [field.name for field in opts.fields]
        else:
            field_names = fields

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(str(opts))

        writer = unicodecsv.writer(response, encoding='utf-8')
        if header:
            writer.writerow(field_names)
        for obj in queryset:
            row = [getattr(obj, field)() if callable(getattr(obj, field)) else getattr(obj, field) for field in
                   field_names]
            writer.writerow(row)
        return response

    export_as_csv.short_description = description
    return export_as_csv
