from django.db import models
import csv
from django.http import HttpResponse

class User(models.Model):
    login = models.CharField(max_length=20, unique=True)
    password = models.CharField(max_length=20)
    name = models.CharField(max_length=30, null=True, blank=True)
    surname = models.CharField(max_length=30, null=True, blank=True)
    second_name = models.CharField(max_length=30, null=True, blank=True)
    reg_date = models.DateField(auto_now_add=True)

    def __str__(self):
        surname = self.surname
        if not surname:
            surname = "login " + self.login
        object_name = "{} {}".format(surname, self.name)
        return object_name

    def get_full_name(self):
        name = self.name.lower().capitalize() if self.name else ""
        surname = self.surname.lower().capitalize() if self.surname else ""
        second_name = self.second_name.lower().capitalize() if self.second_name else ""
        return "{} {} {}".format(surname, name, second_name)

    class Meta:
        ordering = ["surname", "name", "second_name"]
        verbose_name = "Пациент"
        verbose_name_plural = "Пациенты"


class Track(models.Model):
    user = models.ForeignKey(User, null=True)  # on_delete=CASCADE
    add_date = models.DateField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    track_data = models.TextField()
    track_comment = models.TextField(null=True, blank=True)

    def read_track(self):
        track_array = self.track_data.split('\n')
        return track_array

    def __str__(self):
        object_name = "id {} user {}".format(self.id, self.user.login)
        return object_name

    def get_user(self):
        user_login = "{}".format(self.user.login)
        return user_login

    class Meta:
        ordering = ["-updated_date", "user"]
        verbose_name = "Трек"
        verbose_name_plural = "Треки"
