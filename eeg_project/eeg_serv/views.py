# -*- coding: utf-8 -*-
import re
from django.db.models import Q
from django.http import *
from django.shortcuts import render
from eeg_serv.models import User, Track
from django.views.generic import View, ListView
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist

from eeg_serv.utils import receive_data

active_users = set()
track_chunked_id = {}


class TextHttpResponse(HttpResponse):
    content_type = 'text/plain'
    charset = 'UTF-16'


def bad_param(request):
    raise Http404


class LogOut(View):
    def post(self, request):
        login = request.POST["login"]
        try:
            active_users.remove(login)
        except KeyError:
            response = ("user with login " + login + " not log_in")
        else:
            response = ("user  with login " + login + " log_out")
        return HttpResponse(response,
                            content_type='text/plain')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LogOut, self).dispatch(request, *args, **kwargs)


def search(request, name):
    patt = re.compile(r"\s+")
    name = patt.sub(" ", name)
    full_name = name.split()
    users = User.objects.filter(Q(surname__iregex=full_name[0]) |
                                Q(name__iregex=full_name[0]) |
                                Q(second_name__iregex=full_name[0]))
    if len(full_name) == 2:
        users = users.filter(Q(surname__iregex=full_name[1]) |
                             Q(name__iregex=full_name[1]) |
                             Q(second_name__iregex=full_name[1]))
    if len(full_name) == 3:
        users = users.filter(Q(surname__iregex=full_name[2]) |
                             Q(name__iregex=full_name[2]) |
                             Q(second_name__iregex=full_name[2]))

    count_find_users = len(users)
    if count_find_users:
        response = [("<tr><td>" + user.get_full_name() + "</td></tr>") for user in users]  # для вывода в браузере
        # response = [user.get_full_name() for user in users]
        return TextHttpResponse(response,
                                status=302)  # разобраться с кодировкой
    else:
        response = "No users"
        return HttpResponseNotFound(response)


class LogIn(View):
    def post(self, request):
        login = request.POST["login"]
        password = request.POST["password"]
        if login in active_users:
            response = login + " already auth"
            return HttpResponse(response,
                                status=200,
                                content_type='text/plain')
        try:
            user = User.objects.get(login=login, password=password)
        except ObjectDoesNotExist:
            return HttpResponseNotFound("User does not exist or incorrect password",
                                        status=401,
                                        content_type='text/plain')
        else:
            active_users.add(user.login)
            print(len(active_users))
            response = user.name + " auth OK!"
            return HttpResponse(response,
                                status=200,
                                content_type='text/plain')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LogIn, self).dispatch(request, *args, **kwargs)


class UserAdd(View):
    def post(self, request):

        login = request.POST["login"]
        password = request.POST["password"]

        if not password:
            response = "Request has no password"
            return HttpResponseBadRequest(response,
                                          content_type='text/plain')
        if not login:
            response = "Request has no login"
            return HttpResponseBadRequest(response,
                                          content_type='text/plain')
        user_data = request.POST.dict()
        try:
            User.objects.get(login=login)
        except ObjectDoesNotExist:
            new_user = User(**user_data)
            new_user.save()
            response = "User with login " + login + " was created"
            return HttpResponse(response,
                                status=201,
                                content_type='text/plain')
        else:
            response = "This login already used "
            return HttpResponseBadRequest(response,
                                          status=409,
                                          content_type='text/plain')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserAdd, self).dispatch(request, *args, **kwargs)


class EegData(View):
    def post(self, request):
        data_processing = receive_data(request, active_users, track_chunked_id)
        return data_processing

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(EegData, self).dispatch(request, *args, **kwargs)


def tracks(request):
    tracks_list = Track.objects.all()
    return render(request, "tracks.html", {"tracks": tracks_list})


def get_track(request, trackid):
    track = Track.objects.get(id=trackid)
    return render(request, "track.html", {"track": track})


def main(request):
    return render(request, "index.html")
