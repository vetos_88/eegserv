from django.conf.urls import url, include
from eeg_serv.views import *

urlpatterns = [
    url(r'^$', main, name="main"),
    url(r'^log_in', LogIn.as_view(), name="log_in"),
    url(r'^add_new', UserAdd.as_view(), name="add_new"),
    url(r'^log_out', LogOut.as_view(), name="log_out"),
    url(r'^search/(?P<name>.{2,20})', search, name="search"),
    # url(r'^search/(?P<name>.{2,20})', SearchUser.as_view(), name="search"),
    url(r'^data', EegData.as_view(), name="data"),
    url(r'^tracks', tracks, name="tracks"),
    url(r'^track/(?P<trackid>\d+)', get_track, name="get_track"),

]
