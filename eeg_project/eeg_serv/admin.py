from django.contrib import admin
from eeg_serv.admindata import RegexModelAdmin
from eeg_serv.utils import export_as_csv_action
from .models import Track, User


class TrackAdmin(RegexModelAdmin):
    list_display = ('id', 'add_date', 'track_comment', 'updated_date', 'user')
    fields = ('track_comment', 'track_data')
    list_filter = ['updated_date', 'user__login']
    search_fields = ['/user__surname/', '/user__name/', '/user__second_name/', '/user__login/']
    # date_hierarchy = 'updated_date'
    actions = [export_as_csv_action("CSV Export", fields=['id', 'get_user', 'user', 'updated_date', 'track_data'])]
    list_per_page = 10


class UserAdmin(RegexModelAdmin):
    list_display = ('login', 'get_full_name', 'reg_date')
    list_filter = ['reg_date']
    search_fields = ['/name/', '/surname/', '/second_name/']
    list_per_page = 10
    save_as = True
    save_on_top = True


admin.site.register(User, UserAdmin)
admin.site.register(Track, TrackAdmin)
