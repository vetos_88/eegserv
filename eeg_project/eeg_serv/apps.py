from django.apps import AppConfig


class EegServConfig(AppConfig):
    name = 'eeg_serv'
